//The Main setup file to start the server application

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const router = require('./router');
const mongoose = require('mongoose');
const cors = require('cors');

//DB setup
mongoose.connect('mongodb://localhost/auth', {
	useMongoClient: true
});


//app setup
// middlewares
// morgan: For logging each request to the server
// bodyParser: For parsing each request as json.
app.use(morgan('combined'));
app.use(bodyParser.json({type: '*/*'}));

//To Allow CORS(Cross Origin Resource Sharing)
app.use(cors());


//providing app to the router
router(app);


//server setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on:', port);
