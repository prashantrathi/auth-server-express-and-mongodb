const jwt = require('jwt-simple');
const config = require('../config');


const User = require("../models/user");

function tokenForUser(user) {
	const timestamp = new Date().getTime();

	//sub-> subject
	//iat: issued at time
	return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

// signin route to generate token post middleware 'loginStrategy' has validated email/password
exports.signin = function(req, res, next) {
	// User already have email and password . We just have to give token if
	// user is supplying correct email and password

	res.send({ token: tokenForUser(req.user)});
}


// signup route:
// verify email/password is provided
// send error if given email is already in use
// create user and save in Mongodb and provide token to be used for further access
// on the server.
exports.signup = function(req, res, next) {

	// See if the user with the given email exists
	const email = req.body.email;
	const password = req.body.password;

	console.log("Email:", email);
	if(!email || !password) {
		return res.status(422).send({error: 'Both email and password are required'});
	}
	
	// If a user with given email does exist
	User.findOne({ email:email }, function(err, existingUser) {

		console.log("Finding in mongo db");
		if(err) {
			return next(err);
		}

		// httpcode 422 means unprocessable entity i.e we cannot process
		// the data you provided is bad
		if(existingUser) {
			return res.status(422).send({error: 'Email is in use'});
		}

		// If a user with email does NOT exist then create and save the user record
		const user = new User({
			email: email,
			password: password
		});

		user.save(function(err) {
			if(err) {
				return next(err);
			}

			// Respond to the request indicating user is created
			res.json({token: tokenForUser(user)});
		});
		
	});
}