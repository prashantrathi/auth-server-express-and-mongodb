const passport = require('passport');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const User = require('../models/user');
const ExtractJwt = require('passport-jwt').ExtractJwt;
const localStrategy = require('passport-local');


const loginOptions = { usernameField: 'email' };

const localLogin = new localStrategy(loginOptions, function(email, password, done) {

	// verify email and password. 
	// If email and password is correct call done with the user
	// else call done with the false

	User.findOne({email: email}, function(err, user) {

		if(err) { return done(err); }

		if(!user) {
			return done(null, false);
		}

		// compare passwords: comparing user given password with password in db
		user.comparePassword(password, function(err, isMatch) {
			if(err) { return done(err); }

			if(!isMatch) { return done(null, false); }

			return done(null, user);
		});
	});

});


// Setup JWT Strategy options
const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromHeader('authorization'),
	secretOrKey: config.secret
};

// Create JWT Startegy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {

	// See if the User id in the payload exists in our database
	// If it does, call 'done' with that user.
	// otherwise, call done without a user object

	User.findById(payload.sub, function(err, user){

		if(err) { return done(err, false); }

		if(user) {
			return done(null, user);
		}
		else {
			return done(null, false);
		}

	});

});

// Tell passport to use Strategy
passport.use(jwtLogin);
passport.use(localLogin);