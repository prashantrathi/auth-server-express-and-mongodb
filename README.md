# My Project's README

# git clone command
git clone https://bitbucket.org/prashantrathi/auth-server-express-and-mongodb

# username
Username for 'https://bitbucket.org': prashant.8579@gmail.com

# password
Password for 'https://prashant.8579@gmail.com@bitbucket.org': (Enter bitbucket account password)

# STEPS TO RUN THE SERVER

1) cd auth-server-express-and-mongodb

# To install all required modules
2) npm install 

# Do before running the server
3) Create one config.js file inside the “auth-server-express-and-mongodb” folder.
Add following in config.js:

module.exports = {
	secret: “here you can give and combination of some characters to be taken as token secret key”
}

Above file is not checked in. It contains token secret key. This file contains project config and secret keys. It is ignored in .gitignore to not check in.

# Install Mongo DB
4) Install MongoDB from (https://docs.mongodb.com/manual/installation/). Depending upon your platform, select “Install on Linux / Install on MacOS / Install on Windows” and follow the instructions.

# Run the server
5) npm run dev (This will start server listening at port 3090 ).

# Available server routes
Server should be running with following routes:

Root(‘/’), Signin, Signout

**Signin:** Validates passed credentials and returns jwt(JSON WEB TOKEN) to access restricted resource.

* url: localhost:3090/signin
* request data: email/password
* response: { “token” : ”xyz” }

**Signout:** Validates passed credentials are unique and not registered yet. It returns jwt token to access restricted resource.

* url: localhost:3090/signout
* request data: email/password
* response: { “token” : ”xyz” }

**Root(‘/’):** Restricted route. This can be accessed if request has “authorization” header with valid token. Like { “authorization” : “xyz” }

**Note:** Refer package.json file to know which all modules are used in the auth server
