const Authentication = require('./controllers/authentication');
const passportService = require('./services/passport');
const passport = require('passport');

// JWT: JSON WEB TOKEN 
// authentication using jwtStrategy. This is used to authenticate token passed in 
// api for accessing protected resource.
const requireAuth = passport.authenticate('jwt', { session: false} );

// authentication using localStrategy. This is used to authenticate user given email/password
// and provide token
const requireSignIn = passport.authenticate('local', { session: false} );


// exporting function to list different routes
module.exports = function(app) {

	// route to access protected resource post token authentication
	app.get('/', requireAuth, function(req, res) {
		res.send({content: 'My restricted Blog. Only to authenticated user.'});
		
	});

	// route to verify user given email/password and provide token to be used
	// within further calls. This will help to have authenticated access to the server.
	app.post('/signin', requireSignIn, Authentication.signin)

	// route to take user email and password and provide token to be used
	// within further calls. This will help to have authenticated access to the server.
	app.post('/signup', Authentication.signup);
}