const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

//Define model
const userSchema = new Schema({
	email: {type: String, lowercase: true, unique: true},
	password: String
});

//on save hook, encrypt the password

//before saving the model, run this function
userSchema.pre('save', function(next) {

	//get access to the user model
	const user = this;
	
	// generate a salt, then run callback	
	bcrypt.genSalt(10, function(err, salt) {
		if(err) { return next(err); }

		// hash/encrypt the password using the salt
		bcrypt.hash(user.password, salt, null, function(err, hash) {
			if(err) { return next(err); }

			//overide plain text password with encrypted password
			user.password = hash;

			// go ahead and save the model
			next();
		});

	});
});

// any method defined in methods property of schema will be available to each instance of
// the user
userSchema.methods.comparePassword = function(candidatePassword, callback) {

	// candidatePassword: is provided by the user
	// this.password: is the password available in user schema
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {

		if(err) { return callback(err); }

		callback(null, isMatch);
	});
}

//create model class
const ModelClass = mongoose.model('user', userSchema);


//export model class
module.exports = ModelClass;